extends Node2D

onready var image_node = $CanvasLayer/C/Image/ScrollContainer/Control/SImage
onready var zoom_scroll = $CanvasLayer/C/Image/Control/HScrollBar
onready var hour_bar = $CanvasLayer/C/Control/SC/C/HB
onready var time_pointer = $CanvasLayer/C/Control/SC/C/C/TimePointer
onready var time_pointer2 = $CanvasLayer/C/Control/SC/C/C/TimePointer2
var first_point = true

func update_sprite(img: Image, message: String):
	var texture = ImageTexture.new()
	texture.create_from_image(img, 0)

	get_power_sq_data(img)
	image_node.texture = texture
	
	$CanvasLayer/C/Control/UpdateInfo.text = message

func _ready():
	$CanvasLayer/C/MC/VB/SItem/OptionButton.selected = Global.power_group - 1
	

func get_power_sq_data(img):
	img.lock()
	var image_size = img.get_size()
	var v_colors = []
	var line_y_coords = []
	var current_color = Color.white
	var start_color_found = false
	for i in range(image_size.y):
		var px_color = img.get_pixel(10, i)
		if current_color != px_color:
			current_color = px_color
			if !start_color_found:
				if current_color == Color("#778842"):
					start_color_found = true
			else:
				if current_color == Color.white:
					if line_y_coords.size() < Global.power_group:
						line_y_coords.append(i + 2)
				v_colors.append(current_color)
	var line_to_read = Global.power_group - 1
	current_color = Color.white
	var on_color = Color("#c4d09d")
	var off_color = Color("#de7465")
	var power_sq = []
	if line_y_coords.size() > line_to_read:
		for j in range(image_size.x):
			var px_color = img.get_pixel(j, line_y_coords[line_to_read])
			if current_color != px_color:
				current_color = px_color
				if current_color == on_color:
					power_sq.append(1)
				elif current_color == off_color:
					power_sq.append(0)
	if power_sq.size() > 0:
		for i in range(power_sq.size()):
			if power_sq[i]:
				hour_bar.get_child(i).color = on_color
			else:
				hour_bar.get_child(i).color = off_color
	img.unlock()

func update_pointer(hour, minute):
	var hb_size = hour_bar.rect_size
	time_pointer.rect_position.x = hb_size.x / ((24 + 3) * 60) * (hour * 60 + minute + 180) - 5
	time_pointer2.rect_position.x = hb_size.x / ((24 + 3) * 60) * ((hour) * 60 + minute - (21 * 60)) - 5
	return time_pointer.rect_position.x

func _process(delta):
	var time_dict = OS.get_time()
	var hour = time_dict.hour
	var minute = time_dict.minute
	var sec = time_dict.second
	var dots = ":"
	if sec % 2 == 0:
		dots = " "
	var pointer_pos = update_pointer(hour, minute)
	if first_point:
		$CanvasLayer/C/Control/SC.scroll_horizontal = pointer_pos
	$CanvasLayer/Control/Clock.text = str(hour).pad_zeros(2) + dots + str(minute).pad_zeros(2)
	first_point = false

func zoom_image():
	var sc_size = $CanvasLayer/C/Image/ScrollContainer.rect_size
	var scale_value = zoom_scroll.value
	$CanvasLayer/C/Image/ScrollContainer/Control.rect_min_size = Vector2(sc_size.x * scale_value, sc_size.y * scale_value)

func _on_HScrollBar_scrolling():
	zoom_image()


func _on_RefrashButton_pressed():
	Global._ready()


func _on_OptionButton_item_selected(index):
	Global.power_group = index + 1
	Global.save_game()
	Global._ready()
