extends Node
var image_url = "https://oblenergo.cv.ua/shutdowns/GPV.png?ver=1"
var page_url = "https://oblenergo.cv.ua/shutdowns/"

onready var main_node = get_tree().get_root().get_node("Main")
var image_file_path = "user://new_texture.png"
var copy_texture_path = "user://texture_copy.png"
var icon_path = "res://icon.png"

var SAVE_PATH = "user://save_game.save" 

var power_group = 17

func download_texture(url : String, file_name : String):
	var http = HTTPRequest.new()
	http.connect("request_completed", self, "_image_request_completed")
	add_child(http)
	http.set_download_file(file_name)
	http.request(url)
	

func _image_request_completed(result, response_code, headers, body):
	if result == 0:
		update_image()
	else:
		update_image(true)

func update_image(from_copy = false):
	var image = Image.new()
	var dt = Time.get_datetime_string_from_system()
	var message = "Зображення оновлено " + dt
	
	if !from_copy:
		image.load(image_file_path)
		var dir = Directory.new()
		dir.copy(image_file_path, copy_texture_path)
		
	else:
		var directory = Directory.new();
		if directory.file_exists(copy_texture_path):
			image.load(copy_texture_path)
		else:
			image.load(icon_path)
		message = "Оновлення невдале"
	
	main_node.update_sprite(image, message)


func _ready():
	OS.low_processor_usage_mode = true
	load_game()
	call_deferred('download_texture',image_url, image_file_path) 


func save_game():
	var save_dict = {}
	save_dict.power_group = power_group

	var save_file = File.new()
	save_file.open(SAVE_PATH, File.WRITE) 
	save_file.store_line(to_json(save_dict))
	save_file.close()


func load_game():
	var save_file = File.new()
	if !save_file.file_exists(SAVE_PATH):
		return null
	save_file.open(SAVE_PATH, File.READ)
	var data = {}
	data = parse_json(save_file.get_as_text())
	power_group = data.power_group 
	
